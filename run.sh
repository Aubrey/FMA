/bin/mkdir -p ~/fma && chown :1000 ~/fma
input=$1
# use to edit original container 
if [ $input -eq 1 ]; then  
podman run -ti -e DISPLAY --rm -v ~/fma/:/mnt --net=host --name=fma_app localhost/fma:fma_app
elif [ $input -eq 2 ]; then 
	podman pull --tls-verify=false me.aubrey.pro:5000/farm_manager
	podman run -ti -e DISPLAY --rm -v ~/fma/:/mnt --net=host farm_manager
else
# use to run container 
podman pull registry.gitlab.com/aubrey/fma 
podman run -ti -e DISPLAY --rm -v ~/fma/:/mnt --net=host registry.gitlab.com/aubrey/fma:latest
fi

