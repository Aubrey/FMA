# -*- coding: utf-8 -*-
# module for integration functions - bridges UI and DB 

from db_functions import get_categories
from db_functions import get_doc_types
from db_functions import get_connections
from db_functions import get_activedb_name
from db_functions import get_activedb_manager
from db_functions import get_activedb_last_mod
from db_functions import get_documents
from db_functions import get_documents_view_results
from db_functions import convert_fetchall_to_array
from db_functions import app_db_select
from db_functions import app_db_insert


# EVENT HANDLERS AND UI UPDATERS 

def update_create_database_ui(self, event):
    self.create_db_name_text.SetValue("new_farm_db")
    self.create_db_manager_text.SetValue(os.getlogin())
    self.create_db_password_text.SetValue("password")
    self.local_storage_location.SetPath(os.path.expanduser("~"))
    self.db_creation_msg.SetLabel("")
    
def update_dashboard(self):
    print("called update_dashboard")
    self.active_database.SetLabel(get_activedb_name())
    self.database_manager.SetLabel(get_activedb_manager())
    self.database_storage_type.SetLabel("   Local")
    self.database_storage_path.SetLabel(get_activedb_last_mod())

def update_new_record_dropdowns(self):
    # gather choice menu entries from database 
    cats = get_categories()
    docts = get_doc_types()
    conns = get_connections()
    
    # clear choices 
    self.category_selection.Clear()
    self.doc_type_selection.Clear()
    self.connection_selection.Clear()

    # repopulate choices with new entries
    for i in range(len(cats)):
        self.category_selection.Append(cats[i])
    self.category_selection.Append("new...")
    for i in range(len(docts)):
        self.doc_type_selection.Append(docts[i])
    self.doc_type_selection.Append("new...")
    for i in range(len(conns)):
        self.connection_selection.Append(conns[i])
    self.connection_selection.Append("new...")
 
def new_record_to_db(data_in, self):
    # get doc_type_id 
    where_doc_type = "document_type = '" + data_in[0] + "'"
    sel_arr = [
                ['document_type_id'],    # column_array 
                'document_types',   # table 
                where_doc_type,     # where_exp
                ]
    doc_typ_id = convert_fetchall_to_array(app_db_select(sel_arr))
    # get category_id 
    where_cat_name = "category_name = '" + data_in[1] + "'"
    sel_arr = [
                ['category_id'],    # column_array
                'categories',   # table
                where_cat_name,     # where_exp
                ]
    cat_id = convert_fetchall_to_array(app_db_select(sel_arr))
    # get connection_id
    where_con_name = "connection_name = '" + data_in[2] + "'"
    sel_arr = [
                ['connection_id'],    # column_array
                'connections',   # table
                where_con_name,     # where_exp
                ]
    con_id = convert_fetchall_to_array(app_db_select(sel_arr))
    # assemble values into insertion array 
    ins_arr = [
    ['Table', 'documents'],
    ['doc_type_id', str(doc_typ_id[0])],
    ['category_id', str(cat_id[0])],
    ['doc_orig_date', str(data_in[5])],
    ['doc_input_date', str(data_in[4])],
    ['connection_id', str(con_id[0])],
    ['doc_total', str(data_in[3])]
    ] 
    table = "documents"
    status = app_db_insert(ins_arr)
    docs = get_documents()
    # put docs in terminal 
    for row in docs:
        print(row)

    # set feedback labels 
    self.new_record_status_text.SetLabel(status)
    self.new_record_feedback_category.SetLabel(data_in[1])
    self.new_record_feedback_type.SetLabel(data_in[0])
    self.new_record_feedback_date.SetLabel(data_in[4])
    self.new_record_feedback_connection.SetLabel(data_in[2])
    self.new_record_feedback_price.SetLabel(data_in[3])




def set_view_record_data_selection_menu(type, self):
    self.record_view_search_selection.Clear()
    if type == "connections":
        print("replacing search selecitons with connections")
        for i in get_connections():
            self.record_view_search_selection.Append(i)
    if type == "categories":
        print("replacing search selecitons with categories")
        for i in get_categories():
            self.record_view_search_selection.Append(i)
    if type == "document_types":
        print("replacing search selecitons with connections")
        for i in get_doc_types():
            self.record_view_search_selection.Append(i)

def set_records_view_list_values(self, srch_type, where_value, date_type, start_date, end_date):
    if srch_type == "categories":
        where_column = "category_name"
    if srch_type == "connections":
        where_column = "connection_name"
    if srch_type == "document_types":
        where_column = "document_type"
    where_exp = where_column + " = " + "'" + where_value + "'" + " AND doc_" + date_type + " BETWEEN '" + start_date + "' AND '" + end_date + "'"

    query_results = get_documents_view_results(where_exp)
    print(query_results) 
    columns = [
            'doc_id', 
            'type',
            'category',
            'connection',
            'total price',
            'input date',
            'origin date',
            ]
    # clear all existing columns and list items
    self.records_view_list.ClearAll()
    # add in columns
    for column in columns:
        self.records_view_list.AppendColumn(column)
    for rtn_tuple in query_results:
        self.records_view_list.Append(rtn_tuple)
        

    


    
