from PIL import Image
import pytesseract
image = Image.open("images/salvation.jpg")
print("Image sent to tesseract version: " + str(pytesseract.get_tesseract_version()))
print(pytesseract.image_to_string(image ,lang="eng"))
print(pytesseract.image_to_boxes(image ,lang="eng"))
print(pytesseract.image_to_data(image ,lang="eng"))
print(pytesseract.image_to_osd(image ,lang="eng"))
pytesseract.run_and_get_output(image , "output.txt" ) 

