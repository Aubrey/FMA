# -*- coding: utf-8 -*-

import sqlite3 
import sys # system interaction tools
import os
import time
from sqlcipher3 import dbapi2 as sqlcipher

# needed a method to securely connect to db  
def con_app_db_secure(app_db, pw):
    secure_app_db = sqlcipher.connect(app_db)
    pragma_key_pw = "PRAGMA key=" + "\"" + pw + "\""
    secure_app_db.execute(pragma_key_pw)
    return secure_app_db

# insecure - used to manually verify db structure and content
def con_app_db(app_db, pw):
    db = app_db
    db_connection = sqlite3.connect(db)
    cur = db_connection.cursor()
    return cur

# method for system database connection - manage system config via db
def con_sys_db():
    system_db =  "fma_system.db"
    system_db_connection = sqlite3.connect(system_db)
    sys_cur = system_db_connection.cursor()
    return sys_cur

# modular method for ensuring db changes are committed and close db
def commit_and_close(cur):
    cur.execute("COMMIT")
    cur.close()
    return 1

def create_database(path, name, manager, pw):  
    rtn = 0 # initialize return status variable to "no problem"
    # concatenate an absolute file path string for the app db
    app_db = path + "/" + name + ".db"
    try: # test to see if absolute db path is accessible
        cur = con_app_db(app_db, pw)
        cur.close()
    except: # test failed 
        rtn = 1 # set return status to "problem"
    if rtn == 0: # ready to proceed - no problem
        cur = con_app_db(app_db, pw) # connect to app db
        table_array = get_default_app_db_table_array() # self-explanatory code 
        for table in table_array:   # iterate through table array lists
            tablename = ""          # initiate table name variable 
            tna = table.pop(0)      # pop off the table name array
            tablename = tna[1]      # take only the name from the array
            print("_creating db table: " + tablename ) # communicate 
            # make a sql statement from the remaining table and table name 
            sql_create_table = form_sql_statement_create_table(tablename, table)
            cur.execute(sql_create_table) # execute the table creation sql statement
        # register new app db with sys_db AND select the new db for active use 
        # need method for auto-incrementing db number
        insert_into_sys_table("Databases", "", app_db, name, manager, pw)
        insert_into_sys_table("Active", "1", app_db, name, manager, pw)
        app_db_insert_defaults()    # put default data entries into app db
        create_records_view(cur)
        cur.close()
    return rtn

# creates a 3-tier array of tables that constitute a default app db
def get_default_app_db_table_array():
    default_tables = [
    [
    ['Table', 'document_types'],
    ['document_type_id', 'INTEGER', 'NOT NULL', 'PRIMARY KEY', 'AUTOINCREMENT'],
    ['document_type', 'TEXT']
    ],

    [
    ['Table', 'categories'],
    ['category_id', 'INTEGER', 'NOT NULL', 'PRIMARY KEY', 'AUTOINCREMENT'],
    ['category_name', 'TEXT']
    ],

    [
    ['Table', 'zip_codes'],
    ['zip_code', 'INTEGER', 'NOT NULL', 'PRIMARY KEY'],
    ['city', 'TEXT'], 
    ['county', 'TEXT'], 
    ['state', 'TEXT'] 
    ],

    [
    ['Table', 'items'],
    ['item_id', 'INTEGER', 'NOT NULL', 'PRIMARY KEY', 'AUTOINCREMENT'],
    ['category_id', 'TEXT'],
    ['item_name', 'TEXT'],
    ['item_description', 'TEXT']
    ],

    [
    ['Table', 'connections'],
    ['connection_id', 'INTEGER', 'NOT NULL', 'PRIMARY KEY', 'AUTOINCREMENT'],
    ['zip_code', 'INTEGER' ],
    ['connection_name', 'TEXT'],
    ['connection_address', 'TEXT'],
    ['connection_email', 'TEXT']
    ],

    [
    ['Table', 'connection_phones'],
    ['connection_phone_id', 'INTEGER', 'NOT NULL', 'PRIMARY KEY', 'AUTOINCREMENT'],
    ['connection_id', 'INTEGER'],
    ['connection_phone_number', 'TEXT']
    ],
    
    [
    ['Table', 'documents'],
    ['document_id', 'INTEGER', 'NOT NULL', 'PRIMARY KEY', 'AUTOINCREMENT'],
    ['doc_type_id', 'INTEGER'],
    ['category_id', 'INTEGER'],
    ['connection_id', 'INTEGER'],
    ['doc_total', 'NUMERIC'],
    ['doc_input_date', 'TEXT'],
    ['doc_orig_date', 'TEXT']
    ],

    [
    ['Table', 'document_details'],
    ['document_detail_id', 'INTEGER', 'NOT NULL', 'PRIMARY KEY', 'AUTOINCREMENT'],
    ['document_id', 'INTEGER', 'NOT NULL'],
    ['item_id', 'INTEGER'],
    ['quantity', 'INTEGER'],
    ['price', 'NUMERIC']
    ]
    
    ] # end default_tables    
    return default_tables

# SQL STATEMENT CREATION FUNCTIONS 

# function to form a create table sql statement from an array of arrays. 
# takes a table name as a string and columns as a 2-tier array column_options[[col, opt, opt][col, opt, opt].. etc.]
# returns a fully formed sql statement 
def form_sql_statement_create_table(table, column_options):
    statement = "CREATE TABLE IF NOT EXISTS " + table + " ("
    column_option_entries = ""
    for index in range(len(column_options)): # iterate through column options array
        for idx in range(len(column_options[index])): # iterate through each array in the column options array
            column_option_entries += column_options[index][idx] 
            if ( ( idx == len(column_options[index]) -1 ) and ( index < len(column_options) -1 ) ): # 
                column_option_entries += ", "
            column_option_entries += " "
    statement += column_option_entries
    statement += ")"
    return statement 

def create_records_view(cur):
    print("creating records view")
    statement = "CREATE VIEW IF NOT EXISTS records_view AS SELECT document_id, document_type, category_name, connection_name, doc_total, doc_input_date, doc_orig_date from documents INNER JOIN document_types on documents.doc_type_id = document_types.document_type_id INNER JOIN categories on documents.category_id = categories.category_id INNER JOIN connections on documents.connection_id = connections.connection_id"
    print(statement)
    cur.execute(statement)
    return(0)


# function to form an INSERT OR REPLACE  sql statement from a table name and an array of arrays. 
# takes a table name as a string and column - value as a 2-tier array column_value_array[ [column, value], [column, value], [column, value], etc. ]
# returns a fully formed sql "replace" statement - the sqlite parser allows the use of the single keyword REPLACE as an alias for "INSERT OR REPLACE"
def form_sql_statement_insert(table, column_value_array):
    columns = " ("
    values = " ("
    for index in range(len(column_value_array)):
        for idx in range(len(column_value_array[index])):
            # get column from first index 
            if  ( (idx == 0) and ( index < len(column_value_array) -1 ) ): # if not last column
                columns += column_value_array[index][idx]
                columns += ", "
            elif ( (idx == 0) and ( index == len(column_value_array) -1 ) ): # if last column
                columns += column_value_array[index][idx]
                columns += ") "
            elif ( (idx != 0) and ( index < len(column_value_array) -1 ) ): # if not last value
                values += "\'" + column_value_array[index][idx] + "\'"
                values += ", "
            else: # if last value by process of elimination 
                values += "\'" + column_value_array[index][idx] + "\'"
                values += ")"
    statement = "REPLACE INTO " + table + columns + "VALUES " + values
    print(statement)
    return statement 

def form_sql_statement_select(result_columns = ['*'], table = "Table", where_exp = None, group_by_col = None, having_exp = None, values = None, order_by_col = None):
    # initialize return list 
    statement = ""
    status = 0 
    rtn = [statement, status]

    # set scope and initialize assembly strings 
    select = "SELECT "
    columns = ""
    where = ""
    group_by = "" 
    having = ""
    values = ""
    order_by = ""
        
    # assemble columns for use
    for c_idx in range(len(result_columns)):
        columns += result_columns[c_idx]
        if c_idx == (len(result_columns) -1 ): # last column?
            columns += ""
        else: # more columns
            columns += ", "

    # test for where and use if where is there
    if where_exp != None:
        where = "WHERE "
        where += where_exp
        where += " "
    else: 
        where = ""
    
    rtn[0] += select + columns + " FROM " + table + " " + where + group_by + having + values + order_by
    return rtn

def test_form_sql_select():
    print("testing - form_sql_select_statement()")
    cols = ['zip_code', 'city', 'county', 'state']
    table = "zip_codes"
    result = form_sql_statement_select(cols , table) 
    if result[1] == 0:
        print("query is: " + result[0])
        return result[0]
    else: 
        print(result)
        return "Failed"

def app_db_insert_defaults():
    data = get_default_app_db_data()
    cur = con_app_db(get_activedb_path(), get_activedb_pw())
    table = ""
    for ins_data in data:
        if ins_data[0][0] == "Table": # test for table declaration
            t_dat = ins_data.pop(0) # if table - pop off table data entry
            print("Table tuple: " + str(t_dat))
            table = t_dat[1] # set table variable to table data value
        col_val_array = ins_data
        insert_sql = form_sql_statement_insert(table, col_val_array)
        cur.execute(insert_sql)
    commit_and_close(cur)

def get_default_app_db_data(): # generates and returns the default set of app data
    def_app_data = [
        [
        ['Table', 'document_types'],
        ['document_type', 'sale']
        ],
        [ ['document_type', 'purchase'] ],
        [
        ['Table', 'categories'],
        ['category_name', 'feed'] 
        ],
        [ ['category_name', 'seed'] ],
        [ ['category_name', 'produce'] ],
        [ ['category_name', 'fuel'] ],
        [ ['category_name', 'oil'] ],
        [ ['category_name', 'services'] ],
        [ ['category_name', 'hardware'] ],
        [ ['category_name', 'structure'] ],
        [ ['category_name', 'land'] ],
        [ ['category_name', 'equipment'] ],
        [ ['category_name', 'livestock'] ],
        [
        ['Table', 'zip_codes'],
        ['zip_code', '31008'],
        ['city', 'Byron'],
        ['county', 'Peach'],
        ['state', 'GA']
        ],
        [
        ['zip_code', '31217'],
        ['city', 'Macon'],
        ['county', 'Jones'],
        ['state', 'GA']
        ],
        [
        ['Table', 'connections'],
        ['connection_name', 'big creek feed'],
        ['connection_address', '218 Hwy 49 North Suite D'],
        ['zip_code', '31008'],
        ['connection_email', 'bigcreekfeed@gmail.com']
        ],
        [
        ['Table', 'connection_phones'],
        ['connection_id', '1'],
        ['connection_phone_number', '4789560863']
        ], 
        [
        ['Table', 'items'],
        ['category_id', '1'],
        ['item_name', 'Victor Multi-Pro'],
        ['item_description', 'dog food – 50lb']
        ],
        [
        ['category_id', '1'],
        ['item_name', 'ADM 16% medicated'],
        ['item_description', 'buck and doe feed for male goats – 50lb']
        ],
        [
        ['Table', 'documents'],
        ['doc_type_id', '1'],
        ['category_id', '1'],
        ['doc_orig_date', '2019-11-04'], 
        ['doc_input_date', '2019-11-04'], 
        ['connection_id', '1'],
        ['doc_total', '1']
        ],
        [
        ['Table', 'document_details'],
        ['document_id', '1'],
        ['item_id', '1'],
        ['quantity', '5'],  
        ['price', '70.50']
        ]
        ]
    return def_app_data

# test method for inserting data 
def test_app_db_insert():
    col_val_array = [
            ['zip_code', '31217'],
            ['city', 'Macon'],
            ['county', 'Jones'],
            ['state', 'GA'],
            ]
    table = "zip_codes"
    insert_sql = form_sql_statement_insert(table, col_val_array)
    cur = con_app_db(get_activedb_path(), get_activedb_pw())
    cur.execute(insert_sql)
    commit_and_close(cur)


# function to execute selection from application database. 
# Accepts all form_sql_statement_select() arguments as an array 
# default arguments return a list of the tables in the application database.  
def app_db_select(sel_arr = []):
    if len(sel_arr) == 0:
    # default select statement: "SELECT tbl_name FROM sqlite_master WHERE type = 'table'"
        sel_arr = [
                ['tbl_name'],           # column_array 
                'sqlite_master',        # table 
                'type = \'table\'',     # where_exp
                None,                   # group_by_col
                None,                   # having_exp
                None,                   # values
                'tbl_name'              # order_by_col
                ] 
    if len(sel_arr) < 7:
        for ind in range(len(sel_arr),7):
            sel_arr.append(None)
    rtn = [] # init return variable
    sel_sql = form_sql_statement_select(sel_arr[0], sel_arr[1], sel_arr[2], sel_arr[3], sel_arr[4], sel_arr[5], sel_arr[6])
    cur = con_app_db(get_activedb_path(), get_activedb_pw())
    print(sel_sql)
    result = None
    success = 0
    if sel_sql[1] == 0:
        try:
            result = cur.execute(sel_sql[0])
        except:
            success = 1 # indicate failure 
    if success == 0:
        rtn = result.fetchall()
    return rtn
    cur.close()

def test_db_sql(): 
    print("_testing db insert/select")
    test_app_db_insert()
    app_db_select()

def ensure_sys_db():  # tests for presence of system database in working directory and calls creat_sys_db if not exists
    print("check for system db")
    if not(os.path.isfile("fma_system.db")):
        print("system db not found, creating systemdb")
        create_sys_db()
    else:
        print("system db found")
        
def create_sys_db():  # creates a default system database 
    db_table_col_opts = [
    ['Id', 'INTEGER', 'NOT NULL', 'PRIMARY KEY', 'AUTOINCREMENT'], 
    ['Path', 'TEXT'],
    ['Name', 'TEXT'],
    ['Manager', 'TEXT'],
    ['Password', 'TEXT']
    ]
    active_table_col_opts = [
    ['Id', 'INTEGER', 'NOT NULL', 'PRIMARY KEY', 'UNIQUE'],
    ['Path', 'TEXT'],
    ['Name', 'TEXT'],
    ['Manager', 'TEXT'],
    ['Password', 'TEXT']
    ]
    profiles_table_col_opts = [
    ['Id', 'INTEGER', 'NOT NULL', 'PRIMARY KEY', 'UNIQUE', 'AUTOINCREMENT'],
    ['Path', 'TEXT'],
    ['Name', 'TEXT'],
    ['Manager', 'TEXT'],
    ['Password', 'TEXT']
    ]
    
    print("creating system tables")
    sql_sys_table_databases = form_sql_statement_create_table('Databases', db_table_col_opts)
    sql_sys_table_active = form_sql_statement_create_table('Active', active_table_col_opts)
    sql_sys_table_profiles = form_sql_statement_create_table('Profiles', profiles_table_col_opts)

    cur = con_sys_db()
    cur.execute(sql_sys_table_databases)
    cur.execute(sql_sys_table_active)
    cur.execute(sql_sys_table_profiles)

def insert_into_sys_table(table, ident, app_db, name, manager, password): 
    # TODO - encrypt password prior to entry
    ensure_sys_db()
    if not ident: #check for empty ident variable 
        col_val_array = [
            ['Path', app_db],
            ['Name', name],
            ['Manager', manager],
            ['Password', password]
            ]
    else:
        col_val_array = [
            ['Id', ident],
            ['Path', app_db],
            ['Name', name],
            ['Manager', manager],
            ['Password', password]
            ]
    insert_sql = form_sql_statement_insert(table, col_val_array)
    cur = con_sys_db()
    cur.execute(insert_sql)
    commit_and_close(cur)

# ACTIVE DB FUNCTIONS
# these functions only interact with the system db 'Active' table 
# the active table is a single-entry table used for designating and 
#   accessing the application database currently in use 

def select_from_sys_activedb(column, table):
    ensure_sys_db()
    cur = con_sys_db()
    sel = "SELECT " + column + " from " + table + " WHERE Id = '1'"
    result = cur.execute(sel)
    rtn = ""
    try:
        rtn += result.fetchone()[0]
    except:
        rtn += "default"
    cur.close()
    return rtn

# Active DB get functions
def get_activedb_name():
    rtn = "   "
    rtn += select_from_sys_activedb("Name", "Active")
    return rtn

def get_activedb_path():
    rtn = select_from_sys_activedb("Path", "Active")
    return rtn

def get_activedb_manager():
    rtn = "   "
    rtn += select_from_sys_activedb("Manager", "Active")
    return rtn

def get_activedb_pw():
    rtn = ""
    rtn += select_from_sys_activedb("Password", "Active")
    return rtn

def get_activedb_last_mod():
    dbpath = get_activedb_path()
    if dbpath == "default":
        rtn = "   never"
    else:
        rtn = time.ctime(os.path.getmtime(dbpath))
    return rtn


# APPLICATION DB GET FUNCTIONS

def get_categories():
    sel_arr = [
                ['category_name'],  # column_array
                'categories',       # table
                ]
    all_results = app_db_select(sel_arr)
    return convert_fetchall_to_array(all_results)

def get_doc_types():
    sel_arr = [
                ['document_type'],  # column_array
                'document_types',       # table
                ]
    all_results = app_db_select(sel_arr)
    return convert_fetchall_to_array(all_results)

def get_connections():
    sel_arr = [
                ['connection_name'],  # column_array
                'connections',       # table
                ]
    all_results = app_db_select(sel_arr)
    return convert_fetchall_to_array(all_results)

def get_documents():
    # TODO: create documents view to query directly for display of document values 
    sel_arr = [
                ['*'],  # column_array
                'documents',       # table
                ]
    all_results = app_db_select(sel_arr)
    return convert_fetchall_to_tuple_array(all_results)

def get_documents_view_results(where_exp):
    # TODO: create documents view to query directly for display of document values 
    sel_arr = [
                ['*'],           # column_array
                'records_view',        # table
                where_exp,     # where_exp
                ]
    all_results = app_db_select(sel_arr)
    return convert_fetchall_to_tuple_array(all_results)



def convert_fetchall_to_array(all_results):
    rtn = []
    for index in range(len(all_results)):
        for ind in range(len(all_results[index])):
            field = all_results[index][ind]
            rtn.append(field)
    return rtn

def convert_fetchall_to_tuple_array(all_results):
    rtn = []
    for index in range(len(all_results)):
        field = all_results[index]
        rtn.append(field)
    return rtn

# APP DB INSERT FUNCTIONS 

# inserts a single record into the DB
def app_db_insert(data = []):
    if len(data) == 0:
        rtn = "no values provided - provide input"
    cur = con_app_db(get_activedb_path(), get_activedb_pw())
    table = ""
    if data[0][0] == "Table": # test for table declaration
            t_dat = data.pop(0) # if table - pop off table data entry
            print(str(t_dat))
            table = t_dat[1] # set table variable to table data value
    col_val_array = data
    insert_sql = form_sql_statement_insert(table, col_val_array)
    try:
        cur.execute(insert_sql)
        rtn = "data inserted successfully"
    except:
        rtn = "failed to insert data"
    commit_and_close(cur)
    return rtn
