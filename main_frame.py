# -*- coding: UTF-8 -*-
#
# generated by wxGlade 0.9.5 on Thu Aug 20 16:47:56 2020
#

import wx
# begin wxGlade: dependencies
import wx.adv
import wx.aui
import os # os interaction tools 
import sys # system interaction tools 
import time
from datetime import datetime
from db_functions import create_database 
from db_functions import get_activedb_name 
from db_functions import get_activedb_manager
from db_functions import get_activedb_last_mod
from pprint import pprint
import ifunc

# begin wxGlade: extracode
import os
# end wxGlade


class main_frame(wx.Frame):
    def __init__(self, *args, **kwds):
       
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.SetSize((960, 521))
        self.context_menu = wx.Notebook(self, wx.ID_ANY, style=wx.NB_LEFT)
        self.dashboard = wx.Panel(self.context_menu, wx.ID_ANY)
        self.context_menu_Reports = wx.Panel(self.context_menu, wx.ID_ANY)
        self.reports_context = wx.Notebook(self.context_menu_Reports, wx.ID_ANY)
        self.reports_context_GenerateReport = wx.Panel(self.reports_context, wx.ID_ANY)
        self.reports_context_ViewReport = wx.Panel(self.reports_context, wx.ID_ANY)
        self.context_menu_Records = wx.Panel(self.context_menu, wx.ID_ANY)
        self.records_context = wx.Notebook(self.context_menu_Records, wx.ID_ANY)
        self.records_context_create = wx.Panel(self.records_context, wx.ID_ANY)
        # choices populated by query 
        self.category_selection = wx.Choice(self.records_context_create, wx.ID_ANY, choices=ifunc.get_categories())
        self.doc_type_selection = wx.Choice(self.records_context_create, wx.ID_ANY, choices=ifunc.get_doc_types())
        self.doc_orig_date_selection = wx.adv.DatePickerCtrl(self.records_context_create, wx.ID_ANY)
        self.connection_selection = wx.Choice(self.records_context_create, wx.ID_ANY, choices=ifunc.get_connections())
        self.doc_total = wx.TextCtrl(self.records_context_create, wx.ID_ANY, "0.00")
        self.new_record_add_details_btn = wx.Button(self.records_context_create, wx.ID_ANY, "Add Details")
        self.new_record_reset_btn = wx.Button(self.records_context_create, wx.ID_ANY, "Reset")
        self.new_record_commit_btn = wx.Button(self.records_context_create, wx.ID_ANY, "Commit")
        self.records_context_view = wx.Panel(self.records_context, wx.ID_ANY)
        self.window_1 = wx.SplitterWindow(self.records_context_view, wx.ID_ANY)
        self.window_1_pane_1 = wx.Panel(self.window_1, wx.ID_ANY)
        self.records_view_search_type_selection = wx.Choice(self.window_1_pane_1, wx.ID_ANY, choices=["categories", "document_types", "connections"])
        self.record_view_search_selection = wx.Choice(self.window_1_pane_1, wx.ID_ANY, choices=ifunc.get_categories())
        self.records_view_date_type_selection = wx.Choice(self.window_1_pane_1, wx.ID_ANY, choices=["input_date", "orig_date"])
        self.records_view_start_date_selection = wx.adv.DatePickerCtrl(self.window_1_pane_1, wx.ID_ANY)
        self.records_view_end_date_selection = wx.adv.DatePickerCtrl(self.window_1_pane_1, wx.ID_ANY)
        self.records_view_search_button = wx.Button(self.window_1_pane_1, wx.ID_ANY, "search records")
        self.window_1_pane_2 = wx.Panel(self.window_1, wx.ID_ANY)
        self.window_2 = wx.SplitterWindow(self.window_1_pane_2, wx.ID_ANY)
        self.window_2_pane_1 = wx.Panel(self.window_2, wx.ID_ANY)
        self.records_view_list = wx.ListCtrl(self.window_2_pane_1, wx.ID_ANY, style=wx.LC_HRULES | wx.LC_REPORT | wx.LC_VRULES)
        self.window_2_pane_2 = wx.Panel(self.window_2, wx.ID_ANY)
        self.records_view_details_list = wx.ListCtrl(self.window_2_pane_2, wx.ID_ANY, style=wx.LC_HRULES | wx.LC_REPORT | wx.LC_VRULES)
        self.records_context_edit = wx.Panel(self.records_context, wx.ID_ANY)
        self.records_context_delete = wx.Panel(self.records_context, wx.ID_ANY)
        self.context_menu_Categories = wx.Panel(self.context_menu, wx.ID_ANY)
        self.categories_context = wx.Notebook(self.context_menu_Categories, wx.ID_ANY)
        self.categories_context_create = wx.Panel(self.categories_context, wx.ID_ANY)
        self.categories_context_view = wx.Panel(self.categories_context, wx.ID_ANY)
        self.categories_context_edit = wx.Panel(self.categories_context, wx.ID_ANY)
        self.categories_context_delete = wx.Panel(self.categories_context, wx.ID_ANY)
        self.context_menu_Settings = wx.Panel(self.context_menu, wx.ID_ANY)
        self.settings_context = wx.Notebook(self.context_menu_Settings, wx.ID_ANY)
        self.settings_context_Database = wx.Panel(self.settings_context, wx.ID_ANY)
        self.database_settings = wx.Notebook(self.settings_context_Database, wx.ID_ANY)
        self.database_settings_select_active_database = wx.Panel(self.database_settings, wx.ID_ANY)
        self.settings_context_System = wx.Panel(self.settings_context, wx.ID_ANY)
        self.settings_context_OCR = wx.Panel(self.settings_context, wx.ID_ANY)
        self.context_menu_Database = wx.Panel(self.context_menu, wx.ID_ANY)
        self.database_context = wx.Notebook(self.context_menu_Database, wx.ID_ANY)
        self.database_context_control = wx.Panel(self.database_context, wx.ID_ANY)
        self.active_database = wx.StaticText(self.dashboard, wx.ID_ANY, "")
        self.create_db_name_text = wx.TextCtrl(self.database_context_control, wx.ID_ANY, "new_farm_db")
        self.create_db_manager_text = wx.TextCtrl(self.database_context_control, wx.ID_ANY, "")
        self.create_db_password_text = wx.TextCtrl(self.database_context_control, wx.ID_ANY, "password", style=wx.TE_PASSWORD)
        self.use_local_storage_checkbox = wx.CheckBox(self.database_context_control, wx.ID_ANY, "")
        self.local_storage_location = wx.DirPickerCtrl(self.database_context_control, wx.ID_ANY, os.path.expanduser("~"))
        self.use_cloud_storage_checkbox = wx.CheckBox(self.database_context_control, wx.ID_ANY, "")
        self.create_database_reset_btn = wx.Button(self.database_context_control, wx.ID_ANY, "Reset")
        self.create_database_create_btn = wx.Button(self.database_context_control, wx.ID_ANY, "Create")
        self.database_context_UpdateDatabase = wx.Panel(self.database_context, wx.ID_ANY)
        self.database_context_update_search = wx.SearchCtrl(self.database_context_UpdateDatabase, wx.ID_ANY, "")
        self.database_context_DeleteDatabase = wx.Panel(self.database_context, wx.ID_ANY)
        self.database_context_update_search_copy = wx.SearchCtrl(self.database_context_DeleteDatabase, wx.ID_ANY, "")
        self.database_context_Copy = wx.Panel(self.database_context, wx.ID_ANY)
        self.context_menu_Grace = wx.Panel(self.context_menu, wx.ID_ANY)
        self.eternity_context = wx.Notebook(self.context_menu_Grace, wx.ID_ANY)
        self.eternity_context_salvation_copy = wx.Panel(self.eternity_context, wx.ID_ANY)
        self.eternity_sin_hyperlink_1 = wx.adv.HyperlinkCtrl(self.eternity_context_salvation_copy, wx.ID_ANY, "eternity_sin_hyperlink_1", "")
        self.eternity_sin_hyperlink_2 = wx.adv.HyperlinkCtrl(self.eternity_context_salvation_copy, wx.ID_ANY, "eternity_sin_hyperlink_2", "")
        self.eternity_context_salvation = wx.Panel(self.eternity_context, wx.ID_ANY)
        self.eternity_salvation_hyperlink_1 = wx.adv.HyperlinkCtrl(self.eternity_context_salvation, wx.ID_ANY, "eternity_salvation_hyperlink_1", "")
        self.eternity_salvation_hyperlink_2 = wx.adv.HyperlinkCtrl(self.eternity_context_salvation, wx.ID_ANY, "eternity_salvation_hyperlink_2", "")
        self.eternity_context_transformation = wx.Panel(self.eternity_context, wx.ID_ANY)
        self.sanctification_states_hyperlink = wx.adv.HyperlinkCtrl(self.eternity_context_transformation, wx.ID_ANY, "States of Sanctification", "https://chasingthewind.net/category/christian/")
        self.eternity_transformation_hyperlink_2 = wx.adv.HyperlinkCtrl(self.eternity_context_transformation, wx.ID_ANY, "eternity_transformation_hyperlink_2", "")
        self.eternity_context_leadership = wx.Panel(self.eternity_context, wx.ID_ANY)
        self.hyperlink_7 = wx.adv.HyperlinkCtrl(self.eternity_context_leadership, wx.ID_ANY, "eternity_leadership_hyperlink_1", "")
        self.eternity_leadership_hyperlink_2 = wx.adv.HyperlinkCtrl(self.eternity_context_leadership, wx.ID_ANY, "eternity_leadership_hyperlink_2", "")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.handle_new_record_add_details, self.new_record_add_details_btn)
        self.Bind(wx.EVT_BUTTON, self.handle_new_record_reset_btn, self.new_record_reset_btn)
        self.Bind(wx.EVT_BUTTON, self.handle_new_record_commit_btn, self.new_record_commit_btn)
        self.Bind(wx.EVT_BUTTON, self.handle_create_database_reset, self.create_database_reset_btn)
        self.Bind(wx.EVT_BUTTON, self.handle_create_database, self.create_database_create_btn)
        self.Bind(wx.EVT_CHOICE, self.handle_choice_type_selection, self.records_view_search_type_selection)
        self.Bind(wx.EVT_BUTTON, self.handle_search_records, self.records_view_search_button)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.handle_record_list_activated, self.records_view_list)
        self.Bind(wx.EVT_LIST_ITEM_FOCUSED, self.handle_record_list_focused, self.records_view_list)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.handle_record_list_sel, self.records_view_list)

    def __set_properties(self):
        # begin wxGlade: main_frame.__set_properties
        self.SetTitle("Farm Management")
        self.category_selection.SetSelection(0)
        self.doc_type_selection.SetSelection(0)
        self.doc_orig_date_selection.SetMinSize((112, 34))
        self.connection_selection.SetSelection(0)
        self.records_view_search_type_selection.SetSelection(0)
        self.record_view_search_selection.SetSelection(0)
        self.record_view_search_selection.SetMinSize((120, 34))
        self.records_view_date_type_selection.SetSelection(0)
        self.records_view_start_date_selection.SetMinSize((113, 34))
        self.records_view_end_date_selection.SetMinSize((113, 34))
        self.records_view_list.AppendColumn("record_id", format=wx.LIST_FORMAT_LEFT, width=112)
        self.records_view_list.AppendColumn("column 1", format=wx.LIST_FORMAT_LEFT, width=156)
        self.records_view_list.AppendColumn("column 2", format=wx.LIST_FORMAT_LEFT, width=-1)
        self.records_view_list.AppendColumn("etc.", format=wx.LIST_FORMAT_LEFT, width=-1)
        self.records_view_details_list.AppendColumn("detail_id", format=wx.LIST_FORMAT_LEFT, width=112)
        self.records_view_details_list.AppendColumn("column 1", format=wx.LIST_FORMAT_LEFT, width=156)
        self.records_view_details_list.AppendColumn("column 2", format=wx.LIST_FORMAT_LEFT, width=-1)
        self.records_view_details_list.AppendColumn("etc.", format=wx.LIST_FORMAT_LEFT, width=-1)
        self.window_2.SetMinimumPaneSize(20)
        self.window_1.SetMinimumPaneSize(20)
        self.create_db_name_text.SetMinSize((200, 34))
        self.create_db_name_text.SetBackgroundColour(wx.Colour(192, 192, 192))
        self.create_db_name_text.SetForegroundColour(wx.Colour(0, 0, 0))
        self.create_db_name_text.SetToolTip("This will be the name of your new database file")
        self.create_db_manager_text.SetMinSize((200, 34))
        self.create_db_manager_text.SetBackgroundColour(wx.Colour(192, 192, 192))
        self.create_db_manager_text.SetForegroundColour(wx.Colour(0, 0, 0))
        self.create_db_manager_text.SetToolTip("this is the user who will manage the database")
        self.create_db_manager_text.SetValue(os.getlogin())
        self.create_db_password_text.SetMinSize((200, 34))
        self.create_db_password_text.SetBackgroundColour(wx.Colour(192, 192, 192))
        self.create_db_password_text.SetForegroundColour(wx.Colour(0, 0, 0))
        self.create_db_password_text.SetToolTip("choose a password for your database")
        self.use_local_storage_checkbox.SetMinSize((30, 30))
        self.use_local_storage_checkbox.SetToolTip("will store data on local machine.")
        self.use_local_storage_checkbox.SetValue(1)
        self.local_storage_location.SetMinSize((200, 34))
        self.local_storage_location.SetBackgroundColour(wx.Colour(192, 192, 192))
        self.local_storage_location.SetForegroundColour(wx.Colour(0, 0, 0))
        self.local_storage_location.SetToolTip("location where database will be stored")
        self.use_cloud_storage_checkbox.SetMinSize((30, 30))
        self.use_cloud_storage_checkbox.SetToolTip("requires internet and cloud service connection")
        self.create_database_reset_btn.SetMinSize((100, 34))
        self.create_database_create_btn.SetMinSize((100, 34))
        self.database_context_update_search.SetToolTip("search for known databases by database name ")
        self.database_context_update_search.ShowCancelButton(True)
        self.database_context_update_search_copy.ShowCancelButton(True)
        ifunc.update_new_record_dropdowns(self)
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: main_frame.__do_layout
        sizer_5 = wx.BoxSizer(wx.VERTICAL)
        sizer_7 = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer_11 = wx.FlexGridSizer(3, 3, 0, 0)
        grid_sizer_10 = wx.FlexGridSizer(3, 3, 0, 0)
        grid_sizer_9 = wx.FlexGridSizer(3, 2, 0, 0)
        grid_sizer_12 = wx.FlexGridSizer(3, 2, 0, 0)
        sizer_9 = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer_2 = wx.FlexGridSizer(3, 3, 0, 0)
        grid_sizer_1 = wx.FlexGridSizer(3, 3, 0, 0)
        grid_sizer_3 = wx.FlexGridSizer(10, 3, 0, 0)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_8 = wx.BoxSizer(wx.HORIZONTAL)
        database_settings_sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        records_context_sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer_10 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_11 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_14 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_15 = wx.StaticBoxSizer(wx.StaticBox(self.window_2_pane_2, wx.ID_ANY, "Details"), wx.HORIZONTAL)
        sizer_13 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_16 = wx.StaticBoxSizer(wx.StaticBox(self.window_2_pane_1, wx.ID_ANY, "Records"), wx.HORIZONTAL)
        grid_sizer_5 = wx.FlexGridSizer(3, 12, 0, 0)
        new_record_sizer = wx.GridBagSizer(0, 0)
        sizer_6 = wx.BoxSizer(wx.HORIZONTAL)
        dashboard_display = wx.FlexGridSizer(1, 1, 0, 0)
        dashboard_display_status = wx.FlexGridSizer(7, 3, 0, 0)
        database_status = wx.StaticText(self.dashboard, wx.ID_ANY, "Database Status", style=wx.ALIGN_CENTER)
        database_status.SetBackgroundColour(wx.Colour(107, 142, 35))
        database_status.SetFont(wx.Font(14, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, 0, ""))
        dashboard_display_status.Add(database_status, 0, wx.EXPAND, 4)
        blank_label_dbstatus_1 = wx.StaticText(self.dashboard, wx.ID_ANY, "")
        blank_label_dbstatus_1.SetBackgroundColour(wx.Colour(107, 142, 35))
        dashboard_display_status.Add(blank_label_dbstatus_1, 0, wx.EXPAND, 0)
        blank_label_dbstatus_2 = wx.StaticText(self.dashboard, wx.ID_ANY, "")
        blank_label_dbstatus_2.SetBackgroundColour(wx.Colour(107, 142, 35))
        dashboard_display_status.Add(blank_label_dbstatus_2, 0, wx.EXPAND, 0)
        dashboard_display_status.Add((0, 0), 0, 0, 0)
        active_database_label = wx.StaticText(self.dashboard, wx.ID_ANY, "Active Database:", style=wx.ALIGN_RIGHT)
        dashboard_display_status.Add(active_database_label, 0, wx.ALIGN_RIGHT, 0)
        self.active_database = wx.StaticText(self.dashboard, wx.ID_ANY, "")
        self.active_database.SetLabel(get_activedb_name())
        dashboard_display_status.Add(self.active_database, 0, 0, 0)
        dashboard_display_status.Add((0, 0), 0, 0, 0)
        database_manager_label_copy = wx.StaticText(self.dashboard, wx.ID_ANY, "Manager:")
        dashboard_display_status.Add(database_manager_label_copy, 0, wx.ALIGN_RIGHT, 0)
        self.database_manager = wx.StaticText(self.dashboard, wx.ID_ANY, "mgr")
        self.database_manager.SetLabel(get_activedb_manager())
        dashboard_display_status.Add(self.database_manager, 0, 0, 0)
        dashboard_display_status.Add((0, 0), 0, 0, 0)
        database_storage_label = wx.StaticText(self.dashboard, wx.ID_ANY, "Storage Type:")
        dashboard_display_status.Add(database_storage_label, 0, wx.ALIGN_RIGHT, 0)
        self.database_storage_type = wx.StaticText(self.dashboard, wx.ID_ANY, "   Local")
        dashboard_display_status.Add(self.database_storage_type, 0, 0, 0)
        dashboard_display_status.Add((0, 0), 0, 0, 0)
        database_last_update_label = wx.StaticText(self.dashboard, wx.ID_ANY, "Last Updated:")
        dashboard_display_status.Add(database_last_update_label, 0, wx.ALIGN_RIGHT, 0)
        self.database_storage_path = wx.StaticText(self.dashboard, wx.ID_ANY, "")
        self.database_storage_path.SetLabel(get_activedb_last_mod())
        dashboard_display_status.Add(self.database_storage_path, 0, 0, 0)
        system_status_label = wx.StaticText(self.dashboard, wx.ID_ANY, "System Status", style=wx.ALIGN_CENTER)
        system_status_label.SetBackgroundColour(wx.Colour(50, 153, 204))
        system_status_label.SetForegroundColour(wx.Colour(0, 0, 0))
        system_status_label.SetFont(wx.Font(14, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, 0, ""))
        dashboard_display_status.Add(system_status_label, 0, wx.EXPAND, 4)
        blank_label_sysstatus_1 = wx.StaticText(self.dashboard, wx.ID_ANY, "")
        blank_label_sysstatus_1.SetBackgroundColour(wx.Colour(50, 153, 204))
        dashboard_display_status.Add(blank_label_sysstatus_1, 0, wx.EXPAND, 0)
        blank_label_sysstatus_2 = wx.StaticText(self.dashboard, wx.ID_ANY, "")
        blank_label_sysstatus_2.SetBackgroundColour(wx.Colour(50, 153, 204))
        dashboard_display_status.Add(blank_label_sysstatus_2, 0, wx.EXPAND, 0)
        dashboard_display_status.Add((0, 0), 0, 0, 0)
        dashboard_display_status.Add((0, 0), 0, 0, 0)
        dashboard_display_status.Add((0, 0), 0, 0, 0)
        dashboard_display.Add(dashboard_display_status, 1, wx.ALL | wx.EXPAND, 1)
        self.dashboard.SetSizer(dashboard_display)
        self.reports_context.AddPage(self.reports_context_GenerateReport, "Generate Report")
        self.reports_context.AddPage(self.reports_context_ViewReport, "View Report")
        sizer_6.Add(self.reports_context, 1, wx.EXPAND, 0)
        self.context_menu_Reports.SetSizer(sizer_6)
        category_selection_label = wx.StaticText(self.records_context_create, wx.ID_ANY, "Category")
        new_record_sizer.Add(category_selection_label, (0, 0), (1, 1), wx.ALIGN_CENTER | wx.EXPAND, 0)
        doc_type_selection_label = wx.StaticText(self.records_context_create, wx.ID_ANY, "Type")
        new_record_sizer.Add(doc_type_selection_label, (0, 1), (1, 1), 0, 0)
        doc_orig_datepicker_label = wx.StaticText(self.records_context_create, wx.ID_ANY, "Date")
        new_record_sizer.Add(doc_orig_datepicker_label, (0, 2), (1, 1), 0, 0)
        connection_selection_label = wx.StaticText(self.records_context_create, wx.ID_ANY, "Connection")
        new_record_sizer.Add(connection_selection_label, (0, 3), (1, 1), 0, 0)
        document_total_label = wx.StaticText(self.records_context_create, wx.ID_ANY, "Total Price")
        new_record_sizer.Add(document_total_label, (0, 4), (1, 1), wx.ALIGN_CENTER | wx.EXPAND, 0)
        new_record_sizer.Add(self.category_selection, (1, 0), (1, 1), 0, 0)
        new_record_sizer.Add(self.doc_type_selection, (1, 1), (1, 1), 0, 0)
        new_record_sizer.Add(self.doc_orig_date_selection, (1, 2), (1, 1), 0, 0)
        new_record_sizer.Add(self.connection_selection, (1, 3), (1, 1), 0, 0)
        new_record_sizer.Add(self.doc_total, (1, 4), (1, 1), wx.ALIGN_CENTER | wx.EXPAND, 0)
        new_record_sizer.Add(20, 20, (1, 5), (1, 1), wx.EXPAND, 0)
        new_record_sizer.Add(self.new_record_add_details_btn, (1, 6), (1, 1), 0, 0)
        new_record_sizer.Add(20, 20, (2, 3), (1, 1), wx.EXPAND, 0)
        new_record_sizer.Add(20, 20, (2, 4), (1, 1), wx.EXPAND, 0)
        new_record_sizer.Add(self.new_record_reset_btn, (3, 3), (1, 1), 0, 0)
        new_record_sizer.Add(self.new_record_commit_btn, (3, 4), (1, 1), 0, 0)
        new_record_sizer.Add(20, 20, (4, 2), (1, 1), wx.EXPAND, 0)
        new_record_sizer.Add(20, 20, (4, 4), (1, 1), wx.EXPAND, 0)
        new_record_sizer.Add(20, 20, (5, 0), (1, 1), wx.EXPAND, 0)
        self.new_record_status_text = wx.StaticText(self.records_context_create, wx.ID_ANY, "status ")
        new_record_sizer.Add(self.new_record_status_text, (5, 1), (1, 1), 0, 0)
        self.new_record_feedback_category = wx.StaticText(self.records_context_create, wx.ID_ANY, "category")
        new_record_sizer.Add(self.new_record_feedback_category, (6, 0), (1, 1), 0, 0)
        self.new_record_feedback_type = wx.StaticText(self.records_context_create, wx.ID_ANY, "type")
        new_record_sizer.Add(self.new_record_feedback_type, (6, 1), (1, 1), 0, 0)
        self.new_record_feedback_date = wx.StaticText(self.records_context_create, wx.ID_ANY, "date")
        new_record_sizer.Add(self.new_record_feedback_date, (6, 2), (1, 1), 0, 0)
        self.new_record_feedback_connection = wx.StaticText(self.records_context_create, wx.ID_ANY, "")
        new_record_sizer.Add(self.new_record_feedback_connection, (6, 3), (1, 1), 0, 0)
        self.new_record_feedback_price = wx.StaticText(self.records_context_create, wx.ID_ANY, "")
        new_record_sizer.Add(self.new_record_feedback_price, (6, 4), (1, 1), 0, 0)
        self.records_context_create.SetSizer(new_record_sizer)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        record_view_picker_label = wx.StaticText(self.window_1_pane_1, wx.ID_ANY, "Select Criteria")
        grid_sizer_5.Add(record_view_picker_label, 0, wx.ALIGN_CENTER | wx.EXPAND, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        record_view_search_selection_label = wx.StaticText(self.window_1_pane_1, wx.ID_ANY, "Select Value")
        grid_sizer_5.Add(record_view_search_selection_label, 0, wx.ALIGN_CENTER | wx.EXPAND, 0)
        grid_sizer_5.Add((20, 20), 0, 0, 0)
        records_view_date_type_label = wx.StaticText(self.window_1_pane_1, wx.ID_ANY, "Select Date Type")
        grid_sizer_5.Add(records_view_date_type_label, 0, wx.ALIGN_CENTER | wx.EXPAND, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        records_view_start_date_label = wx.StaticText(self.window_1_pane_1, wx.ID_ANY, "Start Date")
        grid_sizer_5.Add(records_view_start_date_label, 0, wx.ALIGN_CENTER | wx.EXPAND, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        records_view_end_date_label = wx.StaticText(self.window_1_pane_1, wx.ID_ANY, "End Date")
        grid_sizer_5.Add(records_view_end_date_label, 0, wx.ALIGN_CENTER | wx.EXPAND, 0)
        grid_sizer_5.Add((20, 20), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add(self.records_view_search_type_selection, 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add(self.record_view_search_selection, 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add(self.records_view_date_type_selection, 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add(self.records_view_start_date_selection, 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add(self.records_view_end_date_selection, 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add(self.records_view_search_button, 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        grid_sizer_5.Add((0, 0), 0, 0, 0)
        self.window_1_pane_1.SetSizer(grid_sizer_5)
        sizer_16.Add(self.records_view_list, 1, wx.EXPAND, 0)
        sizer_13.Add(sizer_16, 1, wx.EXPAND, 0)
        self.window_2_pane_1.SetSizer(sizer_13)
        sizer_15.Add(self.records_view_details_list, 1, wx.EXPAND, 0)
        sizer_14.Add(sizer_15, 1, wx.EXPAND, 0)
        self.window_2_pane_2.SetSizer(sizer_14)
        self.window_2.SplitHorizontally(self.window_2_pane_1, self.window_2_pane_2)
        sizer_11.Add(self.window_2, 1, wx.EXPAND, 0)
        self.window_1_pane_2.SetSizer(sizer_11)
        self.window_1.SplitHorizontally(self.window_1_pane_1, self.window_1_pane_2)
        sizer_10.Add(self.window_1, 1, wx.EXPAND, 0)
        self.records_context_view.SetSizer(sizer_10)
        self.records_context.AddPage(self.records_context_create, "new")
        self.records_context.AddPage(self.records_context_view, "view")
        self.records_context.AddPage(self.records_context_edit, "edit")
        self.records_context.AddPage(self.records_context_delete, "delete")
        records_context_sizer.Add(self.records_context, 1, wx.EXPAND, 0)
        self.context_menu_Records.SetSizer(records_context_sizer)
        self.categories_context.AddPage(self.categories_context_create, "create")
        self.categories_context.AddPage(self.categories_context_view, "view")
        self.categories_context.AddPage(self.categories_context_edit, "edit")
        self.categories_context.AddPage(self.categories_context_delete, "delete")
        sizer_3.Add(self.categories_context, 1, wx.EXPAND, 0)
        self.context_menu_Categories.SetSizer(sizer_3)
        self.database_settings.AddPage(self.database_settings_select_active_database, "select_active_database")
        database_settings_sizer.Add(self.database_settings, 1, wx.EXPAND, 0)
        self.settings_context_Database.SetSizer(database_settings_sizer)
        self.settings_context.AddPage(self.settings_context_Database, "Database")
        self.settings_context.AddPage(self.settings_context_System, "System ")
        self.settings_context.AddPage(self.settings_context_OCR, "OCR")
        sizer_8.Add(self.settings_context, 1, wx.EXPAND, 0)
        self.context_menu_Settings.SetSizer(sizer_8)
        create_database_label = wx.StaticText(self.database_context_control, wx.ID_ANY, "Create Database", style=wx.ALIGN_RIGHT)
        create_database_label.SetBackgroundColour(wx.Colour(107, 142, 35))
        create_database_label.SetFont(wx.Font(14, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, 0, ""))
        grid_sizer_3.Add(create_database_label, 0, wx.EXPAND, 0)
        blank_label_createdb_1 = wx.StaticText(self.database_context_control, wx.ID_ANY, "")
        blank_label_createdb_1.SetBackgroundColour(wx.Colour(107, 142, 35))
        grid_sizer_3.Add(blank_label_createdb_1, 0, wx.EXPAND, 0)
        blank_label_createdb_2 = wx.StaticText(self.database_context_control, wx.ID_ANY, "")
        blank_label_createdb_2.SetBackgroundColour(wx.Colour(107, 142, 35))
        grid_sizer_3.Add(blank_label_createdb_2, 0, wx.EXPAND, 0)
        grid_sizer_3.Add((0, 0), 0, 0, 0)
        create_database_name_label = wx.StaticText(self.database_context_control, wx.ID_ANY, "Database Name: ", style=wx.ALIGN_RIGHT)
        self.db_creation_msg = wx.StaticText(self.database_context_control, wx.ID_ANY, "")
        grid_sizer_3.Add(create_database_name_label, 0, wx.ALIGN_CENTER, 0)
        grid_sizer_3.Add(self.create_db_name_text, 0, wx.EXPAND, 0)
        grid_sizer_3.Add((0, 0), 0, 0, 0)
        create_database_manager_label = wx.StaticText(self.database_context_control, wx.ID_ANY, "Database Manager: ", style=wx.ALIGN_RIGHT)
        grid_sizer_3.Add(create_database_manager_label, 0, wx.ALIGN_CENTER, 0)
        grid_sizer_3.Add(self.create_db_manager_text, 0, wx.EXPAND, 0)
        grid_sizer_3.Add((0, 0), 0, 0, 0)
        create_database_password_label = wx.StaticText(self.database_context_control, wx.ID_ANY, "Database Password: ", style=wx.ALIGN_RIGHT)
        grid_sizer_3.Add(create_database_password_label, 0, wx.ALIGN_CENTER, 0)
        grid_sizer_3.Add(self.create_db_password_text, 0, wx.EXPAND, 0)
        grid_sizer_3.Add((0, 0), 0, 0, 0)
        use_local_storage_label = wx.StaticText(self.database_context_control, wx.ID_ANY, "Use Local Storage: ", style=wx.ALIGN_RIGHT)
        grid_sizer_3.Add(use_local_storage_label, 0, wx.ALIGN_CENTER, 0)
        grid_sizer_3.Add(self.use_local_storage_checkbox, 0, wx.ALIGN_CENTER, 0)
        grid_sizer_3.Add((0, 0), 0, 0, 0)
        local_storage_location_label = wx.StaticText(self.database_context_control, wx.ID_ANY, "Local Storage Location: ", style=wx.ALIGN_RIGHT)
        grid_sizer_3.Add(local_storage_location_label, 0, wx.ALIGN_CENTER, 0)
        grid_sizer_3.Add(self.local_storage_location, 0, wx.EXPAND, 0)
        grid_sizer_3.Add((0, 0), 0, 0, 0)
        use_cloud_storage_label = wx.StaticText(self.database_context_control, wx.ID_ANY, "Use Cloud Storage: ", style=wx.ALIGN_RIGHT)
        grid_sizer_3.Add(use_cloud_storage_label, 0, wx.ALIGN_CENTER, 0)
        grid_sizer_3.Add(self.use_cloud_storage_checkbox, 0, wx.ALIGN_CENTER, 0)
        grid_sizer_3.Add((0, 0), 0, 0, 0)
        grid_sizer_3.Add((20, 20), 0, wx.EXPAND, 0)
        grid_sizer_3.Add((20, 20), 0, wx.EXPAND, 0)
        grid_sizer_3.Add((0, 0), 0, 0, 0)
        sizer_1.Add((100, 20), 0, wx.EXPAND, 0)
        sizer_1.Add(self.create_database_reset_btn, 0, 0, 0)
        grid_sizer_3.Add(sizer_1, 1, wx.EXPAND, 0)
        sizer_2.Add((100, 20), 0, wx.EXPAND, 0)
        sizer_2.Add(self.create_database_create_btn, 0, 0, 0)
        grid_sizer_3.Add(sizer_2, 1, wx.EXPAND, 0)
        grid_sizer_3.Add((0, 0), 0, 0, 0)
        grid_sizer_3.Add((0, 0), 0, 0, 0)
        grid_sizer_3.Add(self.db_creation_msg, 0, wx.EXPAND, 0)
        self.database_context_control.SetSizer(grid_sizer_3)
        grid_sizer_1.Add((0, 0), 0, 0, 0)
        grid_sizer_1.Add(self.database_context_update_search, 0, wx.EXPAND, 0)
        grid_sizer_1.Add((0, 0), 0, 0, 0)
        grid_sizer_1.Add((0, 0), 0, 0, 0)
        grid_sizer_1.Add((0, 0), 0, 0, 0)
        grid_sizer_1.Add((0, 0), 0, 0, 0)
        grid_sizer_1.Add((0, 0), 0, 0, 0)
        grid_sizer_1.Add((0, 0), 0, 0, 0)
        grid_sizer_1.Add((0, 0), 0, 0, 0)
        self.database_context_UpdateDatabase.SetSizer(grid_sizer_1)
        grid_sizer_2.Add((0, 0), 0, 0, 0)
        grid_sizer_2.Add(self.database_context_update_search_copy, 0, wx.EXPAND, 0)
        grid_sizer_2.Add((0, 0), 0, 0, 0)
        grid_sizer_2.Add((0, 0), 0, 0, 0)
        grid_sizer_2.Add((0, 0), 0, 0, 0)
        grid_sizer_2.Add((0, 0), 0, 0, 0)
        grid_sizer_2.Add((0, 0), 0, 0, 0)
        grid_sizer_2.Add((0, 0), 0, 0, 0)
        grid_sizer_2.Add((0, 0), 0, 0, 0)
        self.database_context_DeleteDatabase.SetSizer(grid_sizer_2)
        self.database_context.AddPage(self.database_context_control, "Create ")
        self.database_context.AddPage(self.database_context_UpdateDatabase, "Synchronize")
        self.database_context.AddPage(self.database_context_DeleteDatabase, "Delete ")
        self.database_context.AddPage(self.database_context_Copy, "Clone")
        sizer_9.Add(self.database_context, 1, wx.EXPAND, 0)
        self.context_menu_Database.SetSizer(sizer_9)
        bitmap_7 = wx.StaticBitmap(self.eternity_context_salvation_copy, wx.ID_ANY, wx.Bitmap("images/sin_kills_details.jpg", wx.BITMAP_TYPE_ANY))
        grid_sizer_12.Add(bitmap_7, 0, 0, 0)
        bitmap_8 = wx.StaticBitmap(self.eternity_context_salvation_copy, wx.ID_ANY, wx.Bitmap("images/wages.jpg", wx.BITMAP_TYPE_ANY))
        grid_sizer_12.Add(bitmap_8, 0, 0, 0)
        grid_sizer_12.Add(self.eternity_sin_hyperlink_1, 0, 0, 0)
        grid_sizer_12.Add((0, 0), 0, 0, 0)
        grid_sizer_12.Add(self.eternity_sin_hyperlink_2, 0, 0, 0)
        grid_sizer_12.Add((0, 0), 0, 0, 0)
        self.eternity_context_salvation_copy.SetSizer(grid_sizer_12)
        bitmap_4 = wx.StaticBitmap(self.eternity_context_salvation, wx.ID_ANY, wx.Bitmap("images/salvation_believe.jpg", wx.BITMAP_TYPE_ANY))
        grid_sizer_9.Add(bitmap_4, 0, 0, 0)
        bitmap_9 = wx.StaticBitmap(self.eternity_context_salvation, wx.ID_ANY, wx.Bitmap("images/salvation_grace.jpg", wx.BITMAP_TYPE_ANY))
        grid_sizer_9.Add(bitmap_9, 0, 0, 0)
        grid_sizer_9.Add(self.eternity_salvation_hyperlink_1, 0, 0, 0)
        grid_sizer_9.Add((0, 0), 0, 0, 0)
        grid_sizer_9.Add(self.eternity_salvation_hyperlink_2, 0, 0, 0)
        grid_sizer_9.Add((0, 0), 0, 0, 0)
        self.eternity_context_salvation.SetSizer(grid_sizer_9)
        bitmap_5 = wx.StaticBitmap(self.eternity_context_transformation, wx.ID_ANY, wx.Bitmap("images/sanctify-timothy.jpg", wx.BITMAP_TYPE_ANY))
        grid_sizer_10.Add(bitmap_5, 0, 0, 0)
        grid_sizer_10.Add((0, 0), 0, 0, 0)
        grid_sizer_10.Add((0, 0), 0, 0, 0)
        grid_sizer_10.Add(self.sanctification_states_hyperlink, 0, 0, 0)
        grid_sizer_10.Add((0, 0), 0, 0, 0)
        grid_sizer_10.Add((0, 0), 0, 0, 0)
        grid_sizer_10.Add(self.eternity_transformation_hyperlink_2, 0, 0, 0)
        grid_sizer_10.Add((0, 0), 0, 0, 0)
        grid_sizer_10.Add((0, 0), 0, 0, 0)
        self.eternity_context_transformation.SetSizer(grid_sizer_10)
        bitmap_6 = wx.StaticBitmap(self.eternity_context_leadership, wx.ID_ANY, wx.Bitmap("images/lead_serve.jpg", wx.BITMAP_TYPE_ANY))
        grid_sizer_11.Add(bitmap_6, 0, 0, 0)
        grid_sizer_11.Add((0, 0), 0, 0, 0)
        grid_sizer_11.Add((0, 0), 0, 0, 0)
        grid_sizer_11.Add(self.hyperlink_7, 0, 0, 0)
        grid_sizer_11.Add((0, 0), 0, 0, 0)
        grid_sizer_11.Add((0, 0), 0, 0, 0)
        grid_sizer_11.Add(self.eternity_leadership_hyperlink_2, 0, 0, 0)
        grid_sizer_11.Add((0, 0), 0, 0, 0)
        grid_sizer_11.Add((0, 0), 0, 0, 0)
        self.eternity_context_leadership.SetSizer(grid_sizer_11)
        self.eternity_context.AddPage(self.eternity_context_salvation_copy, "Sin and Death")
        self.eternity_context.AddPage(self.eternity_context_salvation, "Salvation ")
        self.eternity_context.AddPage(self.eternity_context_transformation, "Transformation")
        self.eternity_context.AddPage(self.eternity_context_leadership, "Leadership")
        sizer_7.Add(self.eternity_context, 1, wx.EXPAND, 0)
        self.context_menu_Grace.SetSizer(sizer_7)
        self.context_menu.AddPage(self.dashboard, "Dashboard")
        self.context_menu.AddPage(self.context_menu_Reports, "Reports")
        self.context_menu.AddPage(self.context_menu_Records, "Records")
        self.context_menu.AddPage(self.context_menu_Categories, "Data")
        self.context_menu.AddPage(self.context_menu_Settings, "Settings")
        self.context_menu.AddPage(self.context_menu_Database, "Database")
        self.context_menu.AddPage(self.context_menu_Grace, "Eternity")
        sizer_5.Add(self.context_menu, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_5)
        self.Layout()
        # end wxGlade
        
    # end of class main_frame
    def handle_new_record_reset_btn(self, event):
        ifunc.update_new_record_dropdowns(self)
   
        
    def handle_new_record_commit_btn(self, event):
        # get values from new_record UI 
        new_record_data = []
        new_record_data.append(self.doc_type_selection.GetStringSelection())
        new_record_data.append(self.category_selection.GetStringSelection())
        new_record_data.append(self.connection_selection.GetStringSelection())
        new_record_data.append(self.doc_total.GetLineText(0))
        now = datetime.now()
        today = now.strftime("%Y-%m-%d")
        new_record_data.append(today)
        orig_wx_date = self.doc_orig_date_selection.GetValue()
        doc_orig_date = orig_wx_date.Format("%Y-%m-%d")
        new_record_data.append(doc_orig_date)
        ifunc.new_record_to_db(new_record_data, self)
        # pass values to integration function 
        # receive + display success message for user 
        # display record as entered in DB 
    
    def handle_new_record_add_details(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'handle_new_record_add_details' not implemented!")
        event.Skip()

    def handle_create_database_reset(self, event): 
        print
        self.update_create_database_ui(event)

    def handle_create_database(self, event): 
        msg = "" # initialize user feedback message
        path = self.local_storage_location.GetPath()
        name = self.create_db_name_text.GetLineText(0)
        manager = self.create_db_manager_text.GetLineText(0)
        password = self.create_db_password_text.GetLineText(0)
        if (create_database(path, name, manager, password)) == 0:
            msg = "Database created successfully! New database is active."
            self.db_creation_msg.SetLabel(msg)
            self.db_creation_msg.SetForegroundColour((26, 132, 0)) 
            ifunc.update_dashboard(self)
            ifunc.update_new_record_dropdowns(self)
        else:
            msg = "Database creation failed. Check input values and try again"
            self.db_creation_msg.SetLabel(msg)
            self.db_creation_msg.SetForegroundColour((188, 56, 58))

    def handle_nav_key_event(self, event):
        print("Event handler handle_nav_key_event not implemented!")
        event.Skip()

    def handle_choice_type_selection(self, event):  # wxGlade: main_frame.<event_handler>
        type = self.records_view_search_type_selection.GetString(self.records_view_search_type_selection.GetSelection())
        print("choice type is " + type)
        ifunc.set_view_record_data_selection_menu(type, self)

    def handle_search_records(self, event):  # wxGlade: main_frame.<event_handler>
        # gather values
        srch_type = self.records_view_search_type_selection.GetString(self.records_view_search_type_selection.GetSelection())
        where_value = self.record_view_search_selection.GetString(self.record_view_search_selection.GetSelection())
        date_type = self.records_view_date_type_selection.GetString(self.records_view_date_type_selection.GetSelection())
        start_date = self.records_view_start_date_selection.GetValue().Format("%Y-%m-%d")
        end_date =  self.records_view_end_date_selection.GetValue().Format("%Y-%m-%d")
        
        # call set function 
        ifunc.set_records_view_list_values(self, srch_type, where_value, date_type, start_date, end_date)

        

    def handle_record_list_activated(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'handle_record_list_activated' not implemented!")
        event.Skip()

    def handle_record_list_focused(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'handle_record_list_focused' not implemented!")
        event.Skip()

    def handle_record_list_sel(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'handle_record_list_sel' not implemented!")
        event.Skip()

# end of class main_frame
